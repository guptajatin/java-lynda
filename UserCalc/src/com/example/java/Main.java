package com.example.java;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.print("Enter a number ");
        Scanner input = new Scanner(System.in);
        String first = input.next();
        System.out.print("Enter a number ");
        String second = input.next();
        if (first.contains(".") || second.contains(".")) {
            float f = Float.parseFloat(first);
            float s = Float.parseFloat(second);
            System.out.println(f+s);
        } else {
            int f = Integer.parseInt(first);
            int s = Integer.parseInt(second);
            System.out.println(f+s);
        }
    }
}

package com.example.java;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Enter the items");
        int itemNumber = s.nextInt();
        ArrayList<Grocery> groceryArrayList = new ArrayList<>();
        for (int i = 0; i < itemNumber; i++) {
            System.out.println("Enter the item "+(i+1)+" name");
            String name = s.next();
            System.out.println("Enter the item "+(i+1)+" price");
            float price = s.nextFloat();
            System.out.println("Enter the item "+(i+1)+" quantity");
            float quantity = s.nextFloat();
            groceryArrayList.add(new Grocery(name, price, quantity));
        }

        System.out.println("Enter the items for list 2");
        itemNumber = s.nextInt();

        ArrayList<Grocery> groceryArrayList2 = new ArrayList<>();
        for (int i = 0; i < itemNumber; i++) {
            System.out.println("Enter the item "+(i+1)+" name");
            String name = s.next();
            System.out.println("Enter the item "+(i+1)+" price");
            float price = s.nextFloat();
            System.out.println("Enter the item "+(i+1)+" quantity");
            float quantity = s.nextFloat();
            groceryArrayList2.add(new Grocery(name, price, quantity));
        }
        groceryArrayList.addAll(groceryArrayList2);

        System.out.println("Enter the item name for list one");
        String name = s.next();
        System.out.println("Enter the item price for list one");
        float price = s.nextFloat();
        System.out.println("Enter the item quantity for list one");
        float quantity = s.nextFloat();
        groceryArrayList.add(new Grocery(name, price, quantity));
        System.out.println("Search item name");
        String searchName = s.next();

        Grocery deletingName = null;
        Iterator<Grocery> it = groceryArrayList.iterator();
        while (it.hasNext()) {
            Grocery item = it.next();
            if (item.getItemName().equals(searchName)) {
                deletingName = item;
            }
        }
        groceryArrayList.remove(deletingName);
        groceryArrayList.forEach(item->{
            System.out.println(item.getItemName());
        });



    }
}

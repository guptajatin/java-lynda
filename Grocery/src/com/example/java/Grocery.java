package com.example.java;

public class Grocery {
    private String itemName;
    private float price;
    private float quantity;

    public Grocery(String itemName, float price, float quantity) {
        this.itemName = itemName;
        this.price = price;
        this.quantity = quantity;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}

package com.example.java;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Enter a number ");
        String first = input.next();

        System.out.print("Enter a number ");
        String second = input.next();

        System.out.print("Enter an operator ");
        String op = input.next();

        float f;
        float s;
        try {
            f = Float.parseFloat(first);
            s = Float.parseFloat(second);
            float result;
            switch (op) {
                case "+": System.out.printf("Result : %.2f", f+s);  break;
                case "-": System.out.printf("Result : %.2f", f-s); break;
                case "*": System.out.printf("Result : %.2f", f*s); break;
                case "/": System.out.printf("Result : %.2f", f/s); break;
                default:
                    System.out.println("Wrong operator");
            }
        } catch (NumberFormatException e) {
            System.out.println("The number is not a correct decimal number");
        } catch (NullPointerException e) {
            System.out.println("The value is empty");
        }
    }
}
